import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginSocialComponent } from './login-social/login-social.component';



@NgModule({
  declarations: [LoginSocialComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    RouterModule
  ],
  exports: [LoginSocialComponent]
})
export class ComponentsModule { }
