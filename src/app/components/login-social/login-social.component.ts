import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-social',
  templateUrl: './login-social.component.html',
  styleUrls: ['./login-social.component.scss'],
})
export class LoginSocialComponent implements OnInit {

  email: string;
  password: string;

  constructor(private authService: AuthService, public router: Router) { }

  ngOnInit() {}

  doLogin()
  {
    this.authService.login(this.email, this.password).then( () =>{
      this.router.navigate(['/home']);
    }).catch(err => {
      alert('los datos son incorrectos o no existe el usuario');
    })
  }
  
  loginGoogle(){
    this.authService.loginConGoogle().then( () =>{
      this.router.navigate(['/home']);
    }).catch(err => {
      alert('Error ocurrido: '+err);
    })
  }

}
