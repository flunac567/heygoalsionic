// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyAs5joOMtXHbXrt1TsZenCnEi0vy0ukSiE",
  authDomain: "heygoals-ec1a7.firebaseapp.com",
  projectId: "heygoals-ec1a7",
  storageBucket: "heygoals-ec1a7.appspot.com",
  messagingSenderId: "185119073040",
  appId: "1:185119073040:web:c9ec0e62e0f6ede5d6bb95",
  measurementId: "G-CNCN1V3E2L"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
